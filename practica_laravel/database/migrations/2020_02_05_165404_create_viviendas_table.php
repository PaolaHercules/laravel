<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViviendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viviendas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('c_habit');
            $table->integer('c_banios');
            $table->string('colonia',50);
            $table->float('precio',8,2);
            $table->integer('tamanio');
            $table->string('municipio',50);
            $table->string('departamento',50);
            $table->string('categoria',50);
            $table->string('negociable',50);
            $table->string('estado',50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viviendas');
    }
}
