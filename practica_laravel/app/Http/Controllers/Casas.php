<?php

namespace App\Http\Controllers;

use App\Vivienda;
use Illuminate\Http\Request;


class Casas extends Controller
{
	public function index() {

		$v = Vivienda::all();
		return view('Viviendas', ['viviendas' => $v]);
	}

	public function ingreso(){
		return view('NueVivienda');
	}

	public function store(Request $request)
	{
		Vivienda::create($request->all());
		return redirect()->Route('index');

	}
}


