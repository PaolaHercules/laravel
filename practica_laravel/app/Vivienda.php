<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vivienda extends Model
{
   protected $table = 'viviendas';
   protected $primaryKey = 'id';

   protected $fillable = ['c_habit','c_banios','colonia','precio','tamanio','municipio','departamento','categoria','negociable','estado'];
   public $timestamps = false;

}
