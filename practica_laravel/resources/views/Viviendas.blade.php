<!DOCTYPE html>
<html>
<head>
	<title>Viviendas</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> 

</head>
<body>


	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">Navbar</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="vivienda">Viviendas <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="nuevo">Ingreso</a>
				</li>
			</ul>
			
		</div>
	</nav>

	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-10">
				<h1>Consulta de Viviendas</h1>
			</div>
			
		</div>
	</div>
	<br>

	<div class="container">
		<table class="table table-hover">
			<thead class="thead-dark">
				<tr>
					<th>N habitaciones</th>
					<th>N baños</th>
					<th>Colonia</th>
					<th>Precio</th>
					<th>Tamaño</th>
					<th>Municipio</th>
					<th>Departamento</th>
					<th>Categoria</th>
					<th>Negociable</th>
					<th>Estado</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($viviendas as $c){ ?>
					<tr>
						<td><?= $c->c_habit ?></td>
						<td><?= $c->c_banios ?></td>
						<td><?= $c->colonia ?></td>
						<td><?= $c->precio ?></td>
						<td><?= $c->tamanio ?></td>
						<td><?= $c->municipio ?></td>
						<td><?= $c->departamento ?></td>
						<td><?= $c->categoria ?></td>
						<td><?= $c->negociable ?></td>
						<td><?= $c->estado ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</body>
</html>