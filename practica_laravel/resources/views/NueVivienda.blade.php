<!DOCTYPE html>
<html>
<head>
	<title>Ingresar Vivienda</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">Navbar</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="{{Route('index')}}">Viviendas <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{Route('N')}}">Ingreso</a>
				</li>
			</ul>
		</div>
	</nav>

	<center><br>
		<h1>Ingresar nueva vivienda</h1><br>
		<div class="container">
			<div class="card w-75 p-3">
				<div class="card-body">
					<form method="post" action="{{Route('Nue')}}" autocomplete="off">
						@csrf
						<div class="form-group row">
							<label>Numero de habitaciones</label>
							<input type="number" class="form-control" name="habitacion" >
						</div>
						<div class="form-group row">
							<label>Numero de baños</label>
							<input type="number" class="form-control" name="n_baños" >
						</div>

						<div class="form-group row">
							<label>Colonia</label>
							<input type="text" class="form-control" name="colonia" >
						</div>

						<div class="form-group row">
							<label>Precio</label>
							<input type="number" class="form-control" name="precio" >
						</div>

						<div class="form-group row">
							<label>Tamaño</label>
							<input type="number" class="form-control" name="tamaño" >
						</div>

						<div class="form-group row">
							<label>Minicipio</label>
							<input type="text" class="form-control" name="municipio" >
						</div>

						<div class="form-group row">
							<label>Departamento</label>
							<input type="text" class="form-control" name="departamento" >
						</div>

						<div class="form-group row">
							<label>Categoria</label>
							<input type="text" class="form-control" name="categoria" >
						</div>

						<div class="form-group row">
							<label>¿Es negociable?</label>
							<input type="text" class="form-control" name="negociable" >
						</div>

						<div class="form-group row">
							<label>Estado</label>
							<input type="text" class="form-control" name="estado" >
						</div>	

						<div>
							<input type="submit" class="btn btn-outline-secondary" name="" value="Guardar">
						</div>
					</form>
				</div>
			</div>
		</div>
	</center>
	<br>

</body>
</html>